<?php
/**
 * Created by PhpStorm.
 * User: kubasikorski
 * Date: 26.10.15
 * Time: 22:47
 */
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Chat\Chat;

require dirname(__DIR__) . '/vendor/autoload.php';

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new Chat()
        )
    ),
    8080,
    "0.0.0.0"
);

$server->run();
